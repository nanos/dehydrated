# My dehydrated hooks

You'll need to activate the `allhooks.sh in order to firstly get the DNS challange working, and then secondly deply the hook.

## Cloudflare hook

See the documentation for details.

At a minumum add these to your `config`:

    export CF_EMAIL=michael.thomas.t@gmail.com
    export CF_KEY=[enter your key]
    export CF_DEBUG=true # if you like debugging
    CHALLENGETYPE="dns-01"

## Deploy

Ensure you add your user to the sudoers file using `sudo visudo`: (assuming username `pi`)

    pi ALL=NOPASSWD: /etc/init.d/lighttpd reload