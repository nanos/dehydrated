#!/usr/bin/env bash

# You need to add this to the sudoers file:
# 
# pi ALL=NOPASSWD: /etc/init.d/lighttpd restart

deploy_cert() {
 local DOMAIN="${1}" KEYFILE="${2}" CERTFILE="${3}" FULLCHAINFILE="${4}" CHAINFILE="${5}" TIMESTAMP="${6}"

 echo "DOMAIN : $DOMAIN"
 echo "KEYFILE : $KEYFILE"
 echo "CERTFILE : $CERTFILE"
 echo "FULLCHAINFILE : $FULLCHAINFILE"
 echo "CHAINFILE : $CHAINFILE"

  cat $KEYFILE $CERTFILE > /home/pi/dehydrated/certs/$DOMAIN/combined.pem

  echo "Generated combined.pem"

  # chown www-data -R /home/pi/dehydrated/certs/$DOMAIN
  # chmod 740  -R /home/pi/dehydrated/certs/$DOMAIN

  echo "Set permissions"

  sudo /etc/init.d/lighttpd reload

  echo "Restarted lighttpd"

 # This hook is called once for each certificate that has been
 # produced. Here you might, for instance, copy your new certificates
 # to service-specific locations and reload the service.
 #
 # Parameters:
 # - DOMAIN
 # The primary domain name, i.e. the certificate common
 # name (CN).
 # - KEYFILE
 # The path of the file containing the private key.
 # - CERTFILE
 # The path of the file containing the signed certificate.
 # - FULLCHAINFILE
 # The path of the file containing the full certificate chain.
 # - CHAINFILE
 # The path of the file containing the intermediate certificate(s).
 # - TIMESTAMP
 # Timestamp when the specified certificate was created.
 
 #systemctl reload nginx
 #if [ "$DOMAIN" = "smtp.xxx.xxx" ]
 # then
 # systemctl restart postfix dovecot
 #fi
}

startup_hook() {
  # This hook is called before the cron command to do some initial tasks
  # (e.g. starting a webserver).

  echo "$(date): Starting certificate renewals"
}

exit_hook() {
  # This hook is called at the end of the cron command and can be used to
  # do some final (cleanup or other) tasks.

  echo "$(date): certificate renewals done"
}

HANDLER="$1"; shift

if [[ "${HANDLER}" =~ ^(deploy_cert|startup_hook|exit_hook)$ ]]; then
	"$HANDLER" "$@"
fi