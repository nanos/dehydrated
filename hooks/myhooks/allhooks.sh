#!/usr/bin/env bash
# Simple script which allows the use of multiple hooks
# Put hooks in the order you wish them to be called here:
# (Be sure to keep in mind the working directory *this* hook will be called from when specifying other hook paths.)
# 
/home/pi/dehydrated/hooks/cloudflare/hook.py "$@" # Hook for automatic DNS-01 challenge deployment on Cloudflare
/home/pi/dehydrated/hooks/myhooks/deploy.sh "$@" # Hook to deploy cert and restart lighttpd